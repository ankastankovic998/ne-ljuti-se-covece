#ifndef PLAYER_H
#define PLAYER_H

#include<QVector>
#include "pawn.h"
#include "square.h"
#include "../common/message.h"

class Player : public QObject
{
    Q_OBJECT
private:
    Color color;
    QVector<Pawn*> pawns;
    int startSquare;
    int endSquare;
    QVector<Square*> finishSquares;
    QVector<Square*> startSquares;
    QMap<QString, int> magics;

    void initializeFinishSquares();
    void initializeStartSquares();
    void initMagics();

public:
    explicit Player( Color col = Color::Invalid, int startSquare = 0 , int endSquare = 0, QObject* parent = nullptr );
    Pawn* getPawnByIdx(int idx) const { return pawns[idx]; }
    void updatePawn(Pawn* pawn, int pawnIdx, Square* square);
    void setPawn(Pawn* pawn, int idx) { this->pawns[idx] = pawn; }
    Square* findFreeBaseSquare() const;
    Color getColor() const { return color; }
    int getEndSquare() const { return endSquare ;}
    int getStartSquare() const { return startSquare; }
    Square* getEndingSquare(int idx) { return finishSquares[idx]; }
    Square* getStartingSquare(int idx) const { return startSquares[idx]; }
    bool isMagicAvailable(QString magicName) {return magics.value(magicName, -1) > 0; }
    void activateMagic(QString magicName) { magics[magicName]--; }
    int getNumberOfMagic(QString magicName) const { return magics[magicName]; }
};
#endif // PLAYER_H
