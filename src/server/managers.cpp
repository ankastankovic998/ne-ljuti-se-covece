#include "managers.h"
#include "participants.h"
#include "handlers.h"

#include "../common/messagefactory.h"

#include <QRandomGenerator>

GameManager::GameManager(QString gameCode, QObject *parent)
    : QObject{parent}
{
    this->gameCode = gameCode;
    this->socket = new QTcpSocket();
    this->playerReadyList[0] = false;
    this->playerReadyList[1] = false;
    this->playerReadyList[2] = false;
    this->playerReadyList[3] = false;
    this->turnCount = 0;
    this->turnContext = new TurnContext();
    this->board = new Board(4);
    this->gameStarted = false;
}

Color GameManager::joinGame(BaseParticipant* thread)
{
    if(this->particepants.count() >= 4)
    {
        return Invalid;
    }

    // Ovo bi trebali da lockujemo
    // U slucaju da dva thread-a odrade append istovremeno, onda ce oba thread-a dobiti istu boju
    this->particepants.append(thread);
    return static_cast<Color>(this->particepants.count() -1);
}

int GameManager::rollDice()
{
    this->turnContext->setLastRolledValue(QRandomGenerator::global()->bounded(1,7));
    this->turnContext->useOneRoll();
    return this->turnContext->getLastRolledValue();
}

QPair<QString, QString> GameManager::movePawn(QString fieldName, Color color)
{

    if (turnContext->getLastRolledValue() % 6 == 0) {
        this->turnContext->gainOneRoll();
    } else {
        this->turnContext->useOneMove();
    }

    QPair<QString, QString> result = this->board->makePawnMove(fieldName, color, this->turnContext->getLastRolledValue());
    this->getTurnContext()->setLastRolledValue(0);
    return result;
}

bool GameManager::isValidMove(QString fieldName, Color color)
{
    return this->board->isPawnMoveValid(fieldName, color, this->turnContext->getLastRolledValue());
}

void GameManager::decreaseShieldMagic() {
    board->decreaseCapturable();
}

bool GameManager::isMagicAvailable(QString magicName) const
{
    Color currPlayerColor = this->getTurnContext()->getCurrentPlayerColor();
    Player* currPlayer = this->board->getPlayer(currPlayerColor);
    return currPlayer->isMagicAvailable(magicName);
}

void GameManager::useMagic(QString magicName, qint32 remoteDiceNumber)
{
    qint32 diceNumber = this->getTurnContext()->getLastRolledValue();
    Color currPlayerColor = this->getTurnContext()->getCurrentPlayerColor();
    Player* currPlayer = this->board->getPlayer(currPlayerColor);
    currPlayer->activateMagic(magicName);
    qint32 newDiceNumber = diceNumber;
    if (magicName == "double_dice") {
        newDiceNumber *= 2;
    } else if (magicName == "shield") {
        this->board->setUncapturable(currPlayerColor);
    } else if (magicName == "plus_1") {
        newDiceNumber++;
    } else if (magicName == "minus_1") {
        newDiceNumber--;
    } else if (magicName == "remote_dice" && remoteDiceNumber != -1) {
        newDiceNumber = remoteDiceNumber;
    }
    this->getTurnContext()->setLastRolledValue(newDiceNumber);
    return ;
}

int GameManager::getNumberOfRemainingMagic(QString magicName) const
{
    Color currPlayerColor = this->getTurnContext()->getCurrentPlayerColor();
    Player* currPlayer = this->board->getPlayer(currPlayerColor);
    return currPlayer->getNumberOfMagic(magicName);
}

void GameManager::broadcastResponse(Message* message)
{
    emit sendMessage(message->prepareMessage());
}

bool GameManager::playerIsReady(Color color)
{
    if (this->playerReadyList[color] == true)
    {
        // Ne moze da bude ready ako je vec ready
        qDebug() << "Ne moze da bude ready ako je vec ready";
        return false;
    }

    qDebug() << "Player is ready";
    this->playerReadyList[color] = true;
    return true;
}

bool GameManager::startGame(Color color)
{
    if(color != Color::Blue){
        qDebug() << "Samo kreator igre (Plavi igrac) moze da startuje partiju";
        return false;
    }

    if(this->playerReadyList[Color::Green] == true &&
        this->playerReadyList[Color::Red] == true &&
        this->playerReadyList[Color::Yellow] == true)
    {
        this->playerReadyList[Color::Blue] = true;
        this->gameStarted = true;
        return true;
    }

    qDebug() << "Nisu svi igraci spremni."
             << " Red: " << this->playerReadyList[Color::Red]
             << " Green: " << this->playerReadyList[Color::Green]
             << " Yellow: " << this->playerReadyList[Color::Yellow];

    return false;
}

Color GameManager::nextPlayer(Color color){
    if (color == Yellow){
        this->turnCount += 1;
        return Color::Blue;
    }

    return static_cast<Color>(color+1);
}

Color GameManager::shouldEndGame(){
    return board->isGameFinished();
}

bool GameManager::hasValidMoves(Color playerColor)
{
    return this->board->hasValidPawnMoves(playerColor, this->turnContext->getLastRolledValue());
}

TurnContext *GameManager::getTurnContext() const
{
    return turnContext;
}

bool GameManager::isGameStarted() const
{
    return gameStarted;
}

QList<BaseParticipant *> GameManager::getParticepants() const
{
    return particepants;
}


MessageManager::MessageManager(QObject *parent)
    : QObject{parent}
{
    this->chainElement =
        new ActivateMagicMessageHandler(
            new RollDiceMessageHandler(
                new EndTurnMessageHandler(
                    new StartGameMessageHandler(
                        new PlayerReadyMessageHandler(
                            new JoinGameMessageHandler(
                                new CreateGameMessageHandler(
                                    new MovePawnMessageHandler(
                                        new DefaultHandler()))))))));
}

Response* MessageManager::handleMessage(const QByteArray &rawMessage, BaseParticipant *serverThread){
    Message* message = MessageFactory::createMessage(rawMessage);
    Response* response = this->chainElement->handleMessage(message, serverThread);

    delete message;
    return response;
}

ChainElement *MessageManager::getChainElement() const
{
    return chainElement;
}

