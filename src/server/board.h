#ifndef BOARD_H
#define BOARD_H

#include <QVector>
#include <QMap>
#include <QList>
#include <QPair>
#include "square.h"
#include "player.h"


class Board : public QObject
{
    Q_OBJECT
private:
    QVector<Player*> players;
    QVector<int> uncapturable;

    void initializeTable();
    void addPlayers(int numberOfPlayers);
    Square * tryCapturingEnemyPawn(Square* newSquare, Color currentColor);

    bool validateBasePawnMove(int field, Color color, qint32 diceNumber);
    bool validateFieldPawnMove(int field, Color color, qint32 diceNumber);
    bool validateSafeZonePawnMove(int field, Color color, qint32 diceNumber);

    QPair<QString, QString> makeBasePawnMove(QString name, Color color, qint32 diceNumber);
    QPair<QString, QString> makeFieldPawnMove(QString name, Color color, qint32 diceNumber);
    QPair<QString, QString> makeSafePawnMove(QString name, Color color, qint32 diceNumber);
    bool isCapturable(Color color) const { return uncapturable[color] <= 0; }
public:
    explicit Board(int numberOfPlayers, QObject* parent = nullptr);
    void addPlayer(Color color);
    bool isPawnMoveValid(QString name, Color color, qint32 diceNumber);
    QPair<QString, QString> makePawnMove(QString name, Color color, qint32 diceNumber);
    bool hasValidPawnMoves(Color playerColor, int diceValue);
    Color isGameFinished() const;
    void decreaseCapturable();
    void setUncapturable(Color color) { uncapturable[color] = 4; }
    Player* getPlayer(Color color) const;

    QVector<Square*> table;
};

#endif // BOARD_H
