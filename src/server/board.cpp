#include "board.h"
Board::Board(int numberOfPlayers, QObject *parent)
    :QObject(parent)
{
    this->initializeTable();
    this->addPlayers(numberOfPlayers);
    this->uncapturable = {0, 0, 0, 0, 0};
}

void Board::addPlayers(int numberOfPlayers)
{
    QVector<Color> colors = {Color::Blue, Color::Red, Color::Green, Color::Yellow,};
    for (int i = 0 ; i < numberOfPlayers; i++) {
        this->addPlayer(colors[i]);
    }
}

void Board::addPlayer(Color color)
{
    int mappedColor = (color + 3) % 4;
    int start = 13 * mappedColor;
    int end = 13 * mappedColor - 2 < 0 ? 50 : 13 * mappedColor - 2;
    players.push_back(new Player(color, start, end));
}

bool Board::isPawnMoveValid(QString name, Color color, qint32 diceNumber)
{
    if( ( name[0] == 'b' && color == Color::Blue )  ||
        ( name[0] == 'r' && color == Color::Red )   ||
        ( name[0] == 'g' && color == Color::Green ) ||
        ( name[0] == 'y' && color == Color::Yellow ) )
    {

        int polje = name.mid(name.size() - 1).toInt() - 1;
        return validateBasePawnMove(polje, color, diceNumber);
    }
    else if (name[0] == 'f' )
    {
        int polje = name.mid(1).toInt() - 1;
        return validateFieldPawnMove(polje, color, diceNumber);
    }
    else if( name[0] == 'k' )
    {
        if( ( name[1] == 'b' && color == Color::Blue) ||
            ( name[1] == 'r' && color == Color::Red ) ||
            ( name[1] == 'g' && color == Color::Green ) ||
            ( name[1] == 'y' && color == Color::Yellow ) )
        {
            int polje = name.mid(2).toInt() - 1;

            return validateSafeZonePawnMove(polje, color, diceNumber);
        }
    }
    qDebug() << "Invalid button clicked " << name;
    return false;
}

bool Board::validateBasePawnMove(int polje, Color color, qint32 diceNumber)
{
    Player* currentPlayer = players[color];
    Color baseFieldColor = currentPlayer->getStartingSquare(polje)->getColor();
    Color occupyingColor = table[currentPlayer->getStartSquare()]->getColor();
    return  baseFieldColor == color && occupyingColor != color && diceNumber % 6 == 0 && isCapturable(occupyingColor);
}

bool Board::validateFieldPawnMove(int polje, Color color, qint32 diceNumber)
{
    Player* currentPlayer = players[color];
    int novoPolje = (polje + diceNumber) % 52;
    int endSquareIdx = currentPlayer->getEndSquare();
    if( color ==  Color::Red )
    {
        novoPolje = polje + diceNumber;
    }
    return table[polje]->getColor() == color &&
           ((polje <= endSquareIdx && novoPolje > endSquareIdx && novoPolje < endSquareIdx + 5 &&
             currentPlayer->getEndingSquare(novoPolje - endSquareIdx - 1)->getColor() == Color::Invalid) ||
            (polje > endSquareIdx && table[novoPolje]->getColor() != color && isCapturable(table[novoPolje]->getColor())) ||
            ( polje < endSquareIdx && novoPolje <= endSquareIdx && table[novoPolje]->getColor() != color && isCapturable(table[novoPolje]->getColor())) );
}

bool Board::validateSafeZonePawnMove(int polje, Color color, qint32 diceNumber)
{
    Player* currentPlayer = players[color];

    int novoPolje = polje + diceNumber;

    if (novoPolje > 3)
        return false;
    return (currentPlayer->getEndingSquare(novoPolje)->getColor() == Color::Invalid);
}

QPair<QString, QString> Board::makePawnMove(QString name, Color color, qint32 diceNumber)
{

     if(name[0] == 'r' || name[0] == 'g' || name[0] == 'b' || name[0] == 'y')
    {
        return makeBasePawnMove(name, color, diceNumber);
    }
    else if (name[0] == 'f' )
    {
        return makeFieldPawnMove(name, color, diceNumber);
    }
    else if (name[0] == 'k')
    {
        return makeSafePawnMove(name, color, diceNumber);
    }
    return { "invalidFieldName", "" };
}

Square* Board::tryCapturingEnemyPawn(Square *newSquare, Color currentColor)
{
    if (newSquare->getColor() != currentColor && newSquare->getColor() != Color::Invalid)
    {
        Color enemyColor = newSquare->getColor();
         int enemyPawnIdx = newSquare->getPawnIdx();
        Player* enemyPlayer = this->players[enemyColor];
        Pawn* enemyPawn = enemyPlayer->getPawnByIdx(enemyPawnIdx);
        Square* capturedPawnNewSQuare= enemyPlayer->findFreeBaseSquare();
        enemyPlayer->updatePawn(enemyPawn, enemyPawnIdx, capturedPawnNewSQuare);
        return capturedPawnNewSQuare;
    }
    return nullptr;
}

QPair<QString, QString> Board::makeBasePawnMove(QString name, Color color, qint32 diceNumber)
{
    Player* currentPlayer = players[color];
    int polje = name.mid(name.size() - 1).toInt() - 1;
    int novoPolje = currentPlayer->getStartSquare();
    Square* capturedSquare = tryCapturingEnemyPawn(table[novoPolje], color);
    int currPawnIdx = currentPlayer->getStartingSquare(polje)->getPawnIdx();
    Pawn* currPawn = currentPlayer->getPawnByIdx(currPawnIdx);
    currentPlayer->updatePawn(currPawn, currPawnIdx, table[currentPlayer->getStartSquare()]);
    currentPlayer->getStartingSquare(polje)->updateSquare(Color::Invalid, -1);
     return { table[currentPlayer->getStartSquare()]->getName(), capturedSquare== nullptr ? "" : capturedSquare->getName()};
}

QPair<QString, QString> Board::makeFieldPawnMove(QString name, Color color, qint32 diceNumber)
{
    Player* currentPlayer = players[color];
    int polje = name.mid(1).toInt() - 1;
    int novoPolje = (polje + diceNumber) % 52;
    int endSquareIdx = currentPlayer->getEndSquare();
    if( color ==  Color::Red )
    {
        novoPolje = polje + diceNumber;
    }
    if (polje <= endSquareIdx && novoPolje > endSquareIdx)
    {
        int pawnIdx = table[polje]->getPawnIdx();
        Pawn* pawn = currentPlayer->getPawnByIdx(pawnIdx);
        Square* sq = currentPlayer->getEndingSquare(novoPolje - endSquareIdx - 1);
        currentPlayer->updatePawn(pawn, pawnIdx, sq);
        table[polje]->updateSquare(Color::Invalid, -1);
        return {currentPlayer->getEndingSquare(novoPolje - endSquareIdx - 1)->getName(), ""};
    }
    else
    {
        Square* eatenPawn = tryCapturingEnemyPawn(table[novoPolje], color);
        int currPawnIdx = table[polje]->getPawnIdx();
        Pawn* currPawn = currentPlayer->getPawnByIdx(currPawnIdx);
        currentPlayer->updatePawn(currPawn, currPawnIdx, table[novoPolje]);
        table[polje]->updateSquare(Color::Invalid, -1);
        return {table[novoPolje]->getName(), eatenPawn == nullptr ? "" : eatenPawn->getName()};
    }
}

QPair<QString, QString> Board::makeSafePawnMove(QString name, Color color, qint32 diceNumber)
{
    Player* currentPlayer = players[color];

    int polje = name.mid(2).toInt() - 1;
    int novoPolje = polje + diceNumber;

    Square* sq = currentPlayer->getEndingSquare(polje);
    int pawnIdx = sq->getPawnIdx();
    Pawn* pawn = currentPlayer->getPawnByIdx(pawnIdx);
    currentPlayer->updatePawn(pawn, pawnIdx, currentPlayer->getEndingSquare(novoPolje));
    sq->updateSquare(Color::Invalid, -1);
    return { currentPlayer->getEndingSquare(novoPolje)->getName(), "" };
}

Color Board::isGameFinished() const
{
    bool flag;
    for ( Player* player : players )
    {
        flag = true;
        for ( int i  = 0 ; i<4 ; i++ ) {
            QString polje = player->getPawnByIdx(i)->getSquare()->getName();
            if ( polje[0] != 'k')
            {
                flag = false;
            }
        }
        if(flag)
        {
            return player->getColor();
        }
    }
    return Color::Invalid;
}

Player *Board::getPlayer(Color color) const
{
    return this->players[color];
}

bool Board::hasValidPawnMoves(Color playerColor, int diceValue)
{
    Player* currentPlayer = players[playerColor];

    for(int pIndex = 0; pIndex < 4; pIndex++){
        Pawn* pawn = currentPlayer->getPawnByIdx(pIndex);

        QString fieldName = pawn->getSquare()->getName();
        if (this->isPawnMoveValid(fieldName, playerColor, diceValue)){
            return true;
        }
    }

    return false;
}

void Board::decreaseCapturable() {
    for (int i = 0; i < 4; i++) {
        if (uncapturable[i] > 0) {
            uncapturable[i]--;
        }
    }
}

void Board::initializeTable()
{
    QVector<QString> names = {"f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10",
                              "f11", "f12", "f13", "f14", "f15", "f16", "f17", "f18", "f19", "f20",
                              "f21", "f22", "f23", "f24", "f25", "f26", "f27", "f28", "f29", "f30",
                              "f31", "f32", "f33", "f34", "f35", "f36", "f37", "f38", "f39", "f40",
                              "f41", "f42", "f43", "f44", "f45", "f46", "f47", "f48", "f49", "f50", "f51", "f52", };

    for(QString &name : names){
        this->table.push_back( new Square(name));
    }
}


