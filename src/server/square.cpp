#include "square.h"

Square::Square(QString name, QObject *parent)
    : QObject(parent)
{
    this->name = name;
    this->occupyingPlayerColor = Color::Invalid;
    this->occupyingPawn = -1;
}

Square::Square(QString name, Color color, int pawnId, QObject* parent)
    : QObject(parent)
{
    this->name = name;
    this->occupyingPlayerColor = color;
    this->occupyingPawn = pawnId;
}

Square::~Square()
{

}

void Square::updateSquare(Color color, int occupyingPawn)
{
    this->occupyingPlayerColor = color;
    this->occupyingPawn = occupyingPawn;
}

