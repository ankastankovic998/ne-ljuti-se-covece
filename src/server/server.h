#ifndef SERVER_H
#define SERVER_H

#include "managers.h"
#include <QTcpServer>
#include <QDebug>

class Server : public QTcpServer
{
    Q_OBJECT

public:
    enum Status {
        Success,
        Fail
    };
    static QList<QString> square_names;

    static Server* getInstance();
    GameManager* generateNewGame();
    GameManager* joinGame(QString gameCode);
    Status startServer();

protected:
    void incomingConnection(qintptr socketDescriptor);

private:
    Server(const Server &) = delete;
    Server &operator=(const Server&) = delete;

    QMap<QString, GameManager*>* games;

    QHostAddress HOST = QHostAddress::LocalHost;
    const int PORT = 12345;

    static Server* instance;
    explicit Server(QObject *parent = nullptr);
    ~Server();

    QString generateRandomGameCode();
    QString generateNewGameCode();
    bool gameCodeExists(QString gameCode);

};

#endif // SERVER_H
