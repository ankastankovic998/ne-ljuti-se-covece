#ifndef GAMEPARTICIPANT_H
#define GAMEPARTICIPANT_H

#include <QObject>
#include <QThread>
#include <QTcpSocket>
#include <QDebug>
#include "../common/message.h"
#include "square.h"

class GameManager;
class MessageManager;

class BaseParticipant : public QThread
{
    Q_OBJECT
public:
    explicit BaseParticipant(QObject *parent = nullptr);

    GameManager *gameManager;
    Color color;

public slots:
    virtual void sendMessage(QByteArray message) = 0;

protected:
    void sendResponse(Response* response);

    MessageManager *messageManager;
};

class ServerThreadParticipant : public BaseParticipant
{
    Q_OBJECT
public:

    explicit ServerThreadParticipant(int id, QObject *parent = nullptr);
    ~ServerThreadParticipant();
    void run() override;

signals:
    void error(QTcpSocket::SocketError socketError);

public slots:
    void readyRead();
    void disconnected();

    void sendMessage(QByteArray message) override;

private:
    QTcpSocket *socket;
    int socketDescriptor;
};

class Bot : public BaseParticipant
{
    Q_OBJECT
public:
    explicit Bot(GameManager* gameManager, Color color, QObject *parent = nullptr);

    void run() override;
    void prepareReadyStatus();
    Message* generateNextMove(Response* response);

    Color getColor() const;

public slots:
    void sendMessage(QByteArray message) override;

private:
    Message* generateMessageOnEndTurnResponse(Response *response);
    Message* generateMessageOnRollDiceResponse(Response *response);
    Message* generateMessageOnMovePawnResponse(Response *response);
    Square* randomlySelectPawn();
    bool hasValidMove();

    bool isMyTurn;
};

class BroadcastingThread : public QThread{

    Q_OBJECT

public:

    explicit BroadcastingThread(GameManager* gameManager, Response* response);

    void run() override;

private:
    GameManager* gameManager;
    Response* response;
};

#endif // GAMEPARTICIPANT_H
