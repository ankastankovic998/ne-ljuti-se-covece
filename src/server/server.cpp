#include "server.h"
#include "participants.h"
#include <QRandomGenerator>

Server* Server::instance = nullptr;



Server::Server(QObject *parent)
    : QTcpServer(parent)
{
    this->games = new QMap<QString, GameManager*>();

}

Server::~Server()
{
    delete this->games;
}

Server* Server::getInstance()
{
    if (instance == nullptr){
        instance = new Server();
        instance->startServer();
    }
    return instance;
}

Server::Status Server::startServer()
{
    if (!this->listen(HOST, PORT))
    {
        qDebug() << "Server could not start!";
        return Status::Fail;
    }
    else
    {
        qDebug() << "Server started! Listening...";
        return Status::Success;
    }
}

void Server::incomingConnection(qintptr socketDescriptor)
{
    qDebug() << socketDescriptor << "Connecting... " << "Current server thread :" << QThread::currentThreadId();

    ServerThreadParticipant *thread = new ServerThreadParticipant(socketDescriptor);
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->moveToThread(thread);
    thread->start();
}

QString Server::generateRandomGameCode()
{
    QString randomString;
    const QString validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (int i = 0; i < 7; ++i) {
        int index = QRandomGenerator::global()->bounded(validChars.length());
        randomString.append(validChars.at(index));
    }
    return randomString;
}
QString Server::generateNewGameCode()
{
    QString gameCode;
    do {
        gameCode = generateRandomGameCode();
    }while(gameCodeExists(gameCode));
    return gameCode;
}


bool Server::gameCodeExists(QString gameCode)
{
    return this->games->contains(gameCode);
}

GameManager* Server::joinGame(QString gameCode){
    return this->games->value(gameCode, nullptr);
}

GameManager *Server::generateNewGame()
{
    QString gameCode = generateNewGameCode();
    GameManager* gm = new GameManager(gameCode);
    (*games)[gameCode] = gm;
    return gm;
}
