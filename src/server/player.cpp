#include "player.h"

Player::Player(Color col, int start , int end, QObject* parent)
    :QObject(parent)
{
    this->color = col;
    this->startSquare = start;
    this->endSquare = end;
    this->initializeFinishSquares();
    this->initializeStartSquares();
    this->initMagics();
    for(int i=0 ; i<4 ; i++)
    {
        pawns.push_back(new Pawn(col, this->startSquares[i]));
    }
}

void Player::initMagics()
{
    QVector<QString> magicNames= {"double_dice", "plus_1", "shield", "minus_1", "remote_dice"};
    for (int i = 0; i <magicNames.size(); i++) {
        this->magics[magicNames[i]] = 3;
    }
}

void Player::updatePawn(Pawn *pawn, int pawnIdx, Square *newSquare)
{
    pawn->updateSquare(newSquare);
    newSquare->updateSquare(this->color, pawnIdx);
}

Square *Player::findFreeBaseSquare() const
{
    for (int i = 0; i < 4; i++) {
        if (this->startSquares[i]->getColor() == Color::Invalid) {
            return startSquares[i];
        }
    }
    qDebug() << "noFreeBaseSquare";
    return nullptr;
}

void Player::initializeFinishSquares()
{
    QString c;
    if (this->color == Color::Green){
        c= "kg";
    }
    else if (this->color == Color::Blue){
        c= "kb";
    }
    else if (this->color == Color::Red){
        c= "kr";
    }
    else if (this->color == Color::Yellow){
        c= "ky";
    }

    QVector<QString> names = {"1", "2", "3", "4"};
    for (int i = 0; i < 4; i++) {
        QString name = names[i];
         name.insert(0, c);
        this->finishSquares.push_back( new Square(name));
    }
}

void Player::initializeStartSquares()
{
    QString c;
    if (this->color == Color::Green){
        c= "green";
    }
    else if (this->color == Color::Blue){
        c= "blue";
    }
    else if (this->color == Color::Red){
        c= "red";
    }
    else if (this->color == Color::Yellow){
        c= "yellow";
    }
    QVector<QString> names = {"1", "2", "3", "4"};
    for (int i = 0; i < 4; i++) {
        QString name = names[i];
        name.insert(0, c);
        this->startSquares.push_back( new Square(name, color, i));

    }
}


