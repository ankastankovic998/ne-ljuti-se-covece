#ifndef PAWN_H
#define PAWN_H

#include "square.h"
#include "../common/message.h"

class Pawn : public QObject
{
    Q_OBJECT
private:
    Color color;
    Square *square;
    bool isHome;
    bool isFinished;

public:
    Pawn( Color col , Square* square = nullptr, QObject* parent = nullptr);
    ~Pawn();
//    bool getIsHome() const { return isHome; }
//    bool getIsFinished() const { return isFinished; }
    Color getColor() const { return color; }
//    void leaveHome() { isHome = false; }
//    void sendToHome();
    void updateSquare(Square* square) { this->square = square; }
    Square *getSquare() const;
};

#endif // PAWN_H
