#include "turncontext.h"

TurnContext::TurnContext(QObject *parent)
    : QObject{parent}
{
    this->reset();
}

int TurnContext::getLastRolledValue() const
{
    return lastRolledValue;
}

void TurnContext::setLastRolledValue(int newLastRolledValue)
{
    lastRolledValue = newLastRolledValue;
}

void TurnContext::reset()
{
    this->remainingNumberOfMoves = 1;
    this->remainingNumberOfRolles = 1;
    this->lastRolledValue = 0;
    this->numberOfMagics = 0;
}

void TurnContext::useOneRoll()
{
    this->remainingNumberOfRolles--;
}

void TurnContext::gainOneRoll()
{
    this->remainingNumberOfRolles++;
}

void TurnContext::useOneMove()
{
    this->remainingNumberOfMoves--;
}

void TurnContext::gainOneMove()
{
    this->remainingNumberOfMoves++;
}

Color TurnContext::getCurrentPlayerColor() const
{
    return currentPlayerColor;
}

void TurnContext::setCurrentPlayerColor(Color newCurrentPlayerColor)
{
    currentPlayerColor = newCurrentPlayerColor;
}

int TurnContext::getRemainingNumberOfMoves() const
{
    return remainingNumberOfMoves;
}

void TurnContext::setRemainingNumberOfMoves(int newRemainingNumberOfMoves)
{
    remainingNumberOfMoves = newRemainingNumberOfMoves;
}

int TurnContext::getRemainingNumberOfRolles() const
{
    return remainingNumberOfRolles;
}
