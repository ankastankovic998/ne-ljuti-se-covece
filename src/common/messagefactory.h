#ifndef MESSAGEFACTORY_H
#define MESSAGEFACTORY_H

#include <QObject>
#include "message.h"
#include "common_global.h"

class COMMON_EXPORT MessageFactory : public QObject
{
    Q_OBJECT
public:
    explicit MessageFactory(QObject *parent = nullptr);

    static Message* createMessage(QByteArray value);
signals:

};

#endif // MESSAGEFACTORY_H
