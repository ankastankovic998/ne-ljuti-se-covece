#include "messagefactory.h"
#include <QJsonDocument>

MessageFactory::MessageFactory(QObject *parent)
    : QObject{parent}
{

}

Message* MessageFactory::createMessage(QByteArray value)
{
    QJsonDocument jsonDocument = QJsonDocument::fromJson(value);
    QJsonObject jsonObject = jsonDocument.object();

    QString classType = jsonObject["type"].toString();

    if (classType == CreateGameResponse::TYPE)
    {
        return dynamic_cast<Message*>(new CreateGameResponse(jsonObject));
    }
    else if (classType == CreateGameMessage::TYPE)
    {
        return dynamic_cast<Message*>(new CreateGameMessage(jsonObject));
    }
    else if (classType == JoinGameMessage::TYPE)
    {
        return dynamic_cast<Message*>(new JoinGameMessage(jsonObject));
    }
    else if (classType == JoinGameResponse::TYPE)
    {
        return dynamic_cast<Message*>(new JoinGameResponse(jsonObject));
    }
    else if (classType == PlayerReadyMessage::TYPE)
    {
        return dynamic_cast<Message*>(new PlayerReadyMessage(jsonObject));
    }
    else if (classType == PlayerReadyResponse::TYPE)
    {
        return dynamic_cast<Message*>(new PlayerReadyResponse(jsonObject));
    }
    else if (classType == StartGameMessage::TYPE)
    {
        return dynamic_cast<Message*>(new StartGameMessage(jsonObject));
    }
    else if (classType == StartGameResponse::TYPE)
    {
        return dynamic_cast<Message*>(new StartGameResponse(jsonObject));
    }
    else if (classType == EndTurnMessage::TYPE)
    {
        return dynamic_cast<Message*>(new EndTurnMessage(jsonObject));
    }
    else if (classType == EndTurnResponse::TYPE)
    {
        return dynamic_cast<Message*>(new EndTurnResponse(jsonObject));
    }
    else if (classType == RollDiceMessage::TYPE)
    {
        return dynamic_cast<Message*>(new RollDiceMessage(jsonObject));
    }
    else if (classType == RollDiceResponse::TYPE)
    {
        return dynamic_cast<Message*>(new RollDiceResponse(jsonObject));
    }
    else if (classType == MovePawnMessage::TYPE)
    {
        return dynamic_cast<Message*>(new MovePawnMessage(jsonObject));
    }
    else if (classType == MovePawnResponse::TYPE)
    {
        return dynamic_cast<Message*>(new MovePawnResponse(jsonObject));
    }
    else if (classType == ActivateMagicMessage::TYPE)
    {
        return dynamic_cast<Message*>(new ActivateMagicMessage(jsonObject));
    }
    else if (classType == ActivateMagicResponse::TYPE)
    {
        return dynamic_cast<Message*>(new ActivateMagicResponse(jsonObject));
    }
    else if (classType == EndGameResponse::TYPE)
    {
        return dynamic_cast<Message*>(new EndGameResponse(jsonObject));
    }

    qDebug() << "Nije uspesno kreirana poruka:" << value;
    throw std::runtime_error("Poruka nije uspesno kreirana!");
    return nullptr;
}
