#ifndef TURNCONTEXT_H
#define TURNCONTEXT_H

#include <QObject>
#include "common_global.h"
#include "message.h"


class COMMON_EXPORT TurnContext : public QObject
{
    Q_OBJECT
public:
    explicit TurnContext(QObject *parent = nullptr);

    int getLastRolledValue() const;
    void setLastRolledValue(int newLastRolledValue);

    void reset();

    void useOneRoll();
    void gainOneRoll();

    void useOneMove();
    void gainOneMove();

    Color getCurrentPlayerColor() const;
    void setCurrentPlayerColor(Color newCurrentPlayerColor);

    void increseNumberOfMagics() { numberOfMagics++ ; }
    int getNumberOfMagics() const { return numberOfMagics; }

    int getRemainingNumberOfMoves() const;
    void setRemainingNumberOfMoves(int newRemainingNumberOfMoves);
    int getRemainingNumberOfRolles() const;

private:
    int remainingNumberOfMoves;
    int lastRolledValue;
    int remainingNumberOfRolles;
    Color currentPlayerColor;
    int numberOfMagics;
};

#endif // TURNCONTEXT_H
