#include "catch.hpp"
#include "../common/turncontext.h"
#include "../server/board.h"

TEST_CASE("TurnContext", "[TurnContext]")
{
    TurnContext turnContext;

    SECTION("Inicijalizacija")
    {
        // Arrange
        //

        // Act
        // 

        // Assert
        REQUIRE(turnContext.getRemainingNumberOfMoves() == 1);
        REQUIRE(turnContext.getRemainingNumberOfRolles() == 1);
        REQUIRE(turnContext.getLastRolledValue() == 0);
        REQUIRE(turnContext.getCurrentPlayerColor() == Color::Blue);
    }

    SECTION("Set i Get Last Rolled Value")
    {
        // Arrange
        // 

        // Act
        turnContext.setLastRolledValue(5);

        // Assert
        REQUIRE(turnContext.getLastRolledValue() == 5);
    }

    SECTION("Set i Get Number of Moves")
    {
        // Arrange
        //
        
        // Act

        // Da li treba da testiramo ove jednostavne metode?
        turnContext.gainOneMove();

        // Assert
        REQUIRE(turnContext.getRemainingNumberOfMoves() == 2);
    }

    SECTION("Increment i Decrement Number of Rolles")
    {
        // Arrange
        //

        // Act
        turnContext.gainOneRoll();
        turnContext.gainOneRoll();
        turnContext.useOneRoll();

        // Assert
        REQUIRE(turnContext.getRemainingNumberOfRolles() == 2);
    }

    SECTION("Increment Number of Moves")
    {
        // Arrange
        //
        
        // Act
        turnContext.gainOneMove();

        // Assert
        REQUIRE(turnContext.getRemainingNumberOfMoves() == 2);
    }

    SECTION("Set i Get Current Player Color")
    {
        // Arrange
        //

        // Act
        turnContext.setCurrentPlayerColor(Color::Red);

        // Assert
        REQUIRE(turnContext.getCurrentPlayerColor() == Color::Red);
    }

    SECTION("Reset TurnContext")
    {
        // Arrange
        turnContext.setLastRolledValue(3);
        turnContext.gainOneMove();
        turnContext.gainOneRoll();
        turnContext.setCurrentPlayerColor(Color::Blue);

        // Act
        turnContext.reset();

        // Assert
        REQUIRE(turnContext.getRemainingNumberOfMoves() == 1);
        REQUIRE(turnContext.getRemainingNumberOfRolles() == 1);
        REQUIRE(turnContext.getLastRolledValue() == 0);
        REQUIRE(turnContext.getCurrentPlayerColor() == Color::Blue);
    }
}

//TEST_CASE("Board", "[Board]")
//{

//    SECTION("Pawn movement")
//    {
//        Board board(4);
//        //invalid base pawn move
//        REQUIRE(board.isPawnMoveValid("blue1", Color::Blue, 5) == false);

//        //      base pawn move, no capture
//        REQUIRE(board.isPawnMoveValid("blue1", Color::Blue, 6) == true);
//        REQUIRE(board.makePawnMove("blue1", Color::Blue, 6) == std::pair("f40", nullptr));

//        //    pawn already in the game
//        REQUIRE(board.isPawnMoveValid("blue1", Color::Blue, 6) == false);

//        //    same color pawn occupies start square
//        REQUIRE(board.isPawnMoveValid("blue2", Color::Blue, 6) == false);

//        //    movingPawn
//        REQUIRE(board.isPawnMoveValid("f40", Color::Blue, 4) == true);
//        REQUIRE(board.makePawnMove("f40", Color::Blue, 4) == std::pair("f44", nullptr));
//        REQUIRE(board.isPawnMoveValid("f44", Color::Blue, 5) == true);
//        REQUIRE(board.makePawnMove("f44", Color::Blue, 5) == std::pair("f49", nullptr));
//        REQUIRE(board.isPawnMoveValid("f49", Color::Blue, 4) == true);
//        REQUIRE(board.makePawnMove("f49", Color::Blue, 4) == std::pair("f1", nullptr));

//        //    base pawn move with capture blue
//        REQUIRE(board.isPawnMoveValid("red1", Color::Red, 6) == true);
//        REQUIRE(board.makePawnMove("red1", Color::Red, 6) == std::pair("f1", "blue1"));


//        //    check whether pawn returned to base
//        REQUIRE(board.isPawnMoveValid("blue1", Color::Blue, 6) == true);
//        REQUIRE(board.makePawnMove("blue1", Color::Blue, 6) == std::pair("f40", nullptr));

//        //        moving pawn
//        REQUIRE(board.isPawnMoveValid("f40", Color::Blue, 12) == true);
//        REQUIRE(board.makePawnMove("f40", Color::Blue, 12) == std::pair("f52", nullptr));
//        REQUIRE(board.isPawnMoveValid("f52", Color::Blue, 12) == true);
//        REQUIRE(board.makePawnMove("f52", Color::Blue, 12) == std::pair("f12", nullptr));
//        REQUIRE(board.isPawnMoveValid("f12", Color::Blue, 12) == true);
//        REQUIRE(board.makePawnMove("f12", Color::Blue, 12) == std::pair("f24", nullptr));
//        REQUIRE(board.isPawnMoveValid("f24", Color::Blue, 12) == true);
//        REQUIRE(board.makePawnMove("f24", Color::Blue, 12) == std::pair("f36", nullptr));

//        //        overflow
//        REQUIRE(board.isPawnMoveValid("f36", Color::Blue, 12) == false);

//        //        moving pawn to ending square
//        REQUIRE(board.isPawnMoveValid("f36", Color::Blue, 6) == true);
//        REQUIRE(board.makePawnMove("f36", Color::Blue, 6) == std::pair("kb4", nullptr));

//        //        another pawn for ending square
//        REQUIRE(board.isPawnMoveValid("blue2", Color::Blue, 6) == true);
//        REQUIRE(board.makePawnMove("blue2", Color::Blue, 6) == std::pair("f40", nullptr));
//        REQUIRE(board.makePawnMove("f40", Color::Blue, 12) == std::pair("f52", nullptr));
//        REQUIRE(board.makePawnMove("f52", Color::Blue, 12) == std::pair("f12", nullptr));
//        REQUIRE(board.makePawnMove("f12", Color::Blue, 12) == std::pair("f24", nullptr));
//        REQUIRE(board.makePawnMove("f24", Color::Blue, 12) == std::pair("f36", nullptr));

//        //        moving pawn to ending square that is already occupied
//        REQUIRE(board.isPawnMoveValid("f36", Color::Blue, 6) == false);

//        // moving pawn to ending square
//        REQUIRE(board.isPawnMoveValid("f36", Color::Blue, 4) == true);
//        REQUIRE(board.makePawnMove("f36", Color::Blue, 4) == std::pair("kb2", nullptr));

//        //        moving pawn to already occupied ending square
//        REQUIRE(board.isPawnMoveValid("kb2", Color::Blue, 2) == false);

//        //    overflow of ending square
//        REQUIRE(board.isPawnMoveValid("kb2", Color::Blue, 5) == false);

//        //    moving pawn to ending square
//        REQUIRE(board.isPawnMoveValid("kb2", Color::Blue, 1) == true);
//        REQUIRE(board.makePawnMove("kb2", Color::Blue, 1) == std::pair("kb3", nullptr));

//        //    pawn ending adventure
//        REQUIRE(board.isPawnMoveValid("f521", Color::Blue, 1) == false);
//        //         invalidMove
//        REQUIRE(board.isPawnMoveValid("f40", Color::Blue, 12) == false);
//    }
//}

