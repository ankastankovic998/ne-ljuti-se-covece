#include "catch.hpp"
#include "../common/message.h" 

TEST_CASE("MovePawnMessage", "[MovePawnMessage]")
{
    SECTION("MovePawnMessage toJson pravi JSON objekat sa tipom poruke, poljem i bojom pijuna")
    {
        // Arrange
        MovePawnMessage movePawnMessage("f8", Color::Blue);

        // Act
        QJsonObject resultJson = movePawnMessage.toJson();

        // Assert
        QJsonObject expectedJson;
        expectedJson["type"] = MovePawnMessage::TYPE;
        expectedJson["field"] = "f8";
        expectedJson["pawnColor"] = Color::Blue;

        REQUIRE(resultJson == expectedJson);
    }

    SECTION("MovePawnMessage konstruktor iz QJsonObject inicijalizuje polje i boju pijuna")
    {
        // Arrange
        QJsonObject json;
        json["type"] = MovePawnMessage::TYPE;
        json["field"] = "f8";
        json["pawnColor"] = Color::Red;

        // Act
        MovePawnMessage movePawnMessage(json);

        // Assert
        REQUIRE(movePawnMessage.field == "f8");
        REQUIRE(movePawnMessage.pawnColor == Color::Red);
    }
}

TEST_CASE("MovePawnResponse", "[MovePawnResponse]")
{
    SECTION("MovePawnResponse toJson pravi ocekivane JSON objekte za uspesan odgovor")
    {
        // Arrange
        MovePawnResponse movePawnResponse("f5", Color::Yellow, "f6", "", Color::Invalid);

        // Act
        QJsonObject resultJson = movePawnResponse.toJson();

        // Assert
        QJsonObject expectedJson;
        expectedJson["type"] = MovePawnResponse::TYPE;
        expectedJson["oldField"] = "f5";
        expectedJson["newField"] = "f6";
        expectedJson["eatenPawnNewField"] = "";
        expectedJson["color"] = Color::Yellow;
        expectedJson["eatenColor"] = Color::Invalid;
        expectedJson["success"] = true;
        expectedJson["errorMessage"] = "";

        REQUIRE(resultJson == expectedJson);
    }

    SECTION("MovePawnResponse toJson pravi ocekivane JSON objekte za neuspesan odgovor")
    {
        // Arrange
        MovePawnResponse movePawnResponse(Color::Red, "Error: Invalid move");

        // Act
        QJsonObject resultJson = movePawnResponse.toJson();

        // Assert
        QJsonObject expectedJson;
        expectedJson["type"] = MovePawnResponse::TYPE;
        expectedJson["oldField"] = "";
        expectedJson["newField"] = "";
        expectedJson["eatenPawnNewField"] = "";
        expectedJson["color"] = Color::Red;
        expectedJson["eatenColor"] = Color::Invalid;
        expectedJson["errorMessage"] = "Error: Invalid move";
        expectedJson["success"] = false;

        REQUIRE(resultJson == expectedJson);
    }

    SECTION("MovePawnResponse konstruktor iz QJsonObject inicijalizuje objekte za uspesan odgovor")
    {
        // Arrange
        QJsonObject json;
        json["type"] = MovePawnResponse::TYPE;
        json["oldField"] = "f5";
        json["newField"] = "f10";
        json["eatenPawnNewField"] = "";
        json["color"] = Color::Green;
        json["eatenColor"] = Color::Invalid;
        json["errorMessage"] = "";
        json["success"] = true;

        // Act
        MovePawnResponse movePawnResponse(json);

        // Assert
        REQUIRE(movePawnResponse.getOldField() == "f5");
        REQUIRE(movePawnResponse.getNewField() == "f10");
        REQUIRE(movePawnResponse.getEatenPawnNewField().isEmpty());
        REQUIRE(movePawnResponse.getColor() == Color::Green);
        REQUIRE(movePawnResponse.getEatenColor() == Color::Invalid);
        REQUIRE(movePawnResponse.getErrorMessage().isEmpty());
        REQUIRE(movePawnResponse.getSuccess() == true);
    }

    SECTION("MovePawnResponse konstruktor iz QJsonObject inicijalizuje objekte za neuspesan odgovor")
    {
        // Arrange
        QJsonObject json;
        json["type"] = MovePawnResponse::TYPE;
        json["oldField"] = "";
        json["newField"] = "";
        json["eatenPawnNewField"] = "";
        json["color"] = Color::Invalid;
        json["eatenColor"] = Color::Red;
        json["errorMessage"] = "Error: Invalid move";
        json["success"] = false;

        // Act
        MovePawnResponse movePawnResponse(json);

        // Assert
        REQUIRE(movePawnResponse.getOldField().isEmpty());
        REQUIRE(movePawnResponse.getNewField().isEmpty());
        REQUIRE(movePawnResponse.getEatenPawnNewField().isEmpty());
        REQUIRE(movePawnResponse.getColor() == Color::Invalid);
        REQUIRE(movePawnResponse.getEatenColor() == Color::Red);
        REQUIRE(movePawnResponse.getErrorMessage() == "Error: Invalid move");
        REQUIRE(movePawnResponse.getSuccess() == false);
    }
}
