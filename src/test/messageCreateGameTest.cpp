#include "catch.hpp"
#include "../common/message.h"  


TEST_CASE("CreateGame", "[CreateGame]")
{
	SECTION("CreateGameMessage toJson pravi JSON objekat sa tipom poruke i brojem igraca")
	{
    // Arrange
    CreateGameMessage createGameMessage(4);  //za 4 igraca

    // Act
    QJsonObject resultJson = createGameMessage.toJson();

    // Assert
    QJsonObject expectedJson;
    expectedJson["type"] = CreateGameMessage::TYPE;
    expectedJson["numberOfPlayers"] = 4;

    REQUIRE(resultJson == expectedJson);
	}

	SECTION("CreateGameMessage konstruktor iz QJsonObject inicijalizuje tip poruke i broj igraca")
	{
    // Arrange
    QJsonObject json;
    json["type"] = CreateGameMessage::TYPE;
    json["numberOfPlayers"] = 4;

    // Act
    CreateGameMessage createGameMessage(json);

    // Assert
    REQUIRE(createGameMessage.numberOfPlayers == 4);
	}
	
	
	SECTION("CreateGameResponse toJson pravi ocekivane JSON objekte za uspesan odgovor za crvenog igraca")
	{
	// Arrange
    CreateGameResponse createGameResponse("ABC123", Color::Red);

    // Act
    QJsonObject resultJson = createGameResponse.toJson();

    // Assert
    QJsonObject expectedJson;
    expectedJson["type"] = CreateGameResponse::TYPE;
    expectedJson["gameCode"] = "ABC123";
    expectedJson["color"] = Color::Red;
    expectedJson["errorMessage"] = "";
    expectedJson["success"] = true;

    REQUIRE(resultJson == expectedJson);
	}
	
	SECTION("CreateGameResponse toJson pravi ocekivane JSON objekte za neuspesan odgovor")
	{
	// Arrange
    CreateGameResponse createGameResponse("Error: Game creation failed");

    // Act
    QJsonObject resultJson = createGameResponse.toJson();

    // Assert
    QJsonObject expectedJson;
    expectedJson["type"] = CreateGameResponse::TYPE;
    expectedJson["gameCode"] = "";
    expectedJson["color"] = Color::Invalid;
    expectedJson["errorMessage"] = "Error: Game creation failed";
    expectedJson["success"] = false;

    REQUIRE_FALSE(resultJson == expectedJson);
	}	
	
	SECTION("CreateGameResponse konstruktor iz QJsonObject inicijalizuje objekte za uspesan odgovor")
	{
    // Arrange
    QJsonObject json;
    json["type"] = CreateGameResponse::TYPE;
    json["gameCode"] = "ABC123";
    json["color"] = Color::Blue;
    json["errorMessage"] = "";
    json["success"] = true;

    // Act
    CreateGameResponse createGameResponse(json);

    // Assert
    REQUIRE(createGameResponse.getGameCode() == "ABC123");
    REQUIRE(createGameResponse.getColor() == Color::Blue);
    REQUIRE(createGameResponse.getErrorMessage().isEmpty());
    REQUIRE(createGameResponse.getSuccess() == true);
	}
	
	SECTION("CreateGameResponse konstruktor iz QJsonObject inicijalizuje objekte za neuspesan odgovor")
	{
    // Arrange
    QJsonObject json;
    json["type"] = CreateGameResponse::TYPE;
    json["gameCode"] = "";
    json["color"] = Color::Invalid;
    json["errorMessage"] = "Error: Game creation failed";
    json["success"] = false;

    // Act
    CreateGameResponse createGameResponse(json);

    // Assert
    REQUIRE(createGameResponse.getGameCode().isEmpty());
    REQUIRE(createGameResponse.getColor() == Color::Invalid);
    REQUIRE(createGameResponse.getErrorMessage() == "Error: Game creation failed");
    REQUIRE(createGameResponse.getSuccess() == false);
	}	
}
