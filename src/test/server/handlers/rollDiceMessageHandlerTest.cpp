#include "../../catch.hpp"
#include "../../../common/message.h"
#include "../../../server/participants.h"
#include "../../../server/server.h"
#include "../../../server/handlers.h"
#include "../../../server/managers.h"

TEST_CASE("RollDiceMessageHandlerTest", "[RollDiceMessageHandlerTest]")
{
    SECTION("RollDiceMessageHandlerTest : Uspesno bacena kockica")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        prvi->gameManager = gameManager;
        prvi->color = prvi->gameManager->joinGame(prvi);
        prvi->gameManager->playerReadyList[0] = true;

        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        drugi->gameManager = gameManager;
        drugi->color = drugi->gameManager->joinGame(drugi);
        drugi->gameManager->playerReadyList[1] = true;

        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        treci->gameManager = gameManager;
        treci->color = treci->gameManager->joinGame(treci);
        treci->gameManager->playerReadyList[2] = true;

        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);
        cetvrti->gameManager = gameManager;
        cetvrti->color = cetvrti->gameManager->joinGame(cetvrti);
        cetvrti->gameManager->playerReadyList[3] = true;

        gameManager->getTurnContext()->setCurrentPlayerColor(prvi->color);

        RollDiceMessage rdm = RollDiceMessage();

        RollDiceMessageHandler* rdmh = new RollDiceMessageHandler(new DefaultHandler());

        Response *response = rdmh->handleMessage(&rdm, prvi);
        RollDiceResponse* rdr = dynamic_cast<RollDiceResponse*>(response);

        REQUIRE(rdr != nullptr);

        REQUIRE(rdr->getSuccess() == true);
        REQUIRE(rdr->shouldBroadcast() == true);
        REQUIRE(rdr->getColor() == prvi->color);
        REQUIRE(rdr->getErrorMessage() == "");
    }

    SECTION("RollDiceMessageHandlerTest : Ne mozete baciti kockicu, jer niste na potezu!")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        prvi->gameManager = gameManager;
        prvi->color = prvi->gameManager->joinGame(prvi);
        prvi->gameManager->playerReadyList[0] = true;

        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        drugi->gameManager = gameManager;
        drugi->color = drugi->gameManager->joinGame(drugi);
        drugi->gameManager->playerReadyList[1] = true;

        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        treci->gameManager = gameManager;
        treci->color = treci->gameManager->joinGame(treci);
        treci->gameManager->playerReadyList[2] = true;

        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);
        cetvrti->gameManager = gameManager;
        cetvrti->color = cetvrti->gameManager->joinGame(cetvrti);
        cetvrti->gameManager->playerReadyList[3] = true;

        gameManager->getTurnContext()->setCurrentPlayerColor(drugi->color);

        RollDiceMessage rdm = RollDiceMessage();

        RollDiceMessageHandler* rdmh = new RollDiceMessageHandler(new DefaultHandler());

        Response *response = rdmh->handleMessage(&rdm, prvi);
        RollDiceResponse* rdr = dynamic_cast<RollDiceResponse*>(response);

        REQUIRE(rdr != nullptr);

        REQUIRE(rdr->getSuccess() == false);
        REQUIRE(rdr->shouldBroadcast() == false);
        REQUIRE(rdr->getColor() == prvi->color);
        REQUIRE(rdr->getErrorMessage() == "Ne mozete baciti kockicu, jer niste na potezu!");
    }

    SECTION("RollDiceMessageHandlerTest : Ispunili ste dozvoljen broj bacanja kockice!")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        prvi->gameManager = gameManager;
        prvi->color = prvi->gameManager->joinGame(prvi);
        prvi->gameManager->playerReadyList[0] = true;

        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        drugi->gameManager = gameManager;
        drugi->color = drugi->gameManager->joinGame(drugi);
        drugi->gameManager->playerReadyList[1] = true;

        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        treci->gameManager = gameManager;
        treci->color = treci->gameManager->joinGame(treci);
        treci->gameManager->playerReadyList[2] = true;

        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);
        cetvrti->gameManager = gameManager;
        cetvrti->color = cetvrti->gameManager->joinGame(cetvrti);
        cetvrti->gameManager->playerReadyList[3] = true;

        gameManager->getTurnContext()->setCurrentPlayerColor(prvi->color);
        gameManager->getTurnContext()->useOneRoll();

        RollDiceMessage rdm = RollDiceMessage();

        RollDiceMessageHandler* rdmh = new RollDiceMessageHandler(new DefaultHandler());

        Response *response = rdmh->handleMessage(&rdm, prvi);
        RollDiceResponse* rdr = dynamic_cast<RollDiceResponse*>(response);

        REQUIRE(rdr != nullptr);

        REQUIRE(rdr->getSuccess() == false);
        REQUIRE(rdr->shouldBroadcast() == false);
        REQUIRE(rdr->getColor() == prvi->color);
        REQUIRE(rdr->getErrorMessage() == "Ispunili ste dozvoljen broj bacanja kockice!");
    }
}
