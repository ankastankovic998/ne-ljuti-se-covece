#include "../../catch.hpp"
#include "../../../common/message.h"
#include "../../../server/participants.h"
#include "../../../server/server.h"
#include "../../../server/handlers.h"
#include "../../../server/managers.h"

TEST_CASE("JoinGameHandlerTest", "[JoinGameMessageHandler]")
{
    SECTION("JoinGameHandlerTest : Uspesno pridruzivanje partiji")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        JoinGameMessage jgm = JoinGameMessage(gameManager->gameCode);

        JoinGameMessageHandler* jgmh = new JoinGameMessageHandler(new DefaultHandler());
        Response *response = jgmh->handleMessage(&jgm, serverThread);

        JoinGameResponse* jgr = dynamic_cast<JoinGameResponse*>(response);
        REQUIRE(jgr != nullptr);

        REQUIRE(jgr->getJoined() == true);

        REQUIRE(serverThread->gameManager != nullptr);
    }

    SECTION("JoinGameHandlerTest : Vec si pridruzen partiji!")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        JoinGameMessage jgm = JoinGameMessage(gameManager->gameCode);

        JoinGameMessageHandler* jgmh = new JoinGameMessageHandler(new DefaultHandler());
        Response *response = jgmh->handleMessage(&jgm, serverThread);

        JoinGameResponse* jgr = dynamic_cast<JoinGameResponse*>(response);
        REQUIRE(jgr != nullptr);

        REQUIRE(jgr->getJoined() == true);

        REQUIRE(serverThread->gameManager != nullptr);

        JoinGameMessage jgm1 = JoinGameMessage(gameManager->gameCode);
        Response *response1 = jgmh->handleMessage(&jgm, serverThread);

        JoinGameResponse* jgr1 = dynamic_cast<JoinGameResponse*>(response1);
        REQUIRE(jgr1 != nullptr);

        REQUIRE(jgr1->getErrorMessage() == "Vec si pridruzen partiji!");
    }

    SECTION("JoinGameHandlerTest : Partija ne postoji!")
    {
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        JoinGameMessage jgm = JoinGameMessage("los kod # & %");

        JoinGameMessageHandler* jgmh = new JoinGameMessageHandler(new DefaultHandler());
        Response *response = jgmh->handleMessage(&jgm, serverThread);

        JoinGameResponse* jgr = dynamic_cast<JoinGameResponse*>(response);
        REQUIRE(jgr != nullptr);

        REQUIRE(jgr->getJoined() == false);
        REQUIRE(jgr->getSuccess() == false);
        REQUIRE(jgr->getErrorMessage() == "Partija ne postoji!");

        REQUIRE(serverThread->gameManager == nullptr);
    }

    SECTION("JoinGameHandlerTest : Partija je puna!")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);

        gameManager->joinGame(prvi);
        gameManager->joinGame(drugi);
        gameManager->joinGame(treci);
        gameManager->joinGame(cetvrti);

        ServerThreadParticipant *serverThread = new ServerThreadParticipant(5, nullptr);
        JoinGameMessage jgm = JoinGameMessage(gameManager->gameCode);

        JoinGameMessageHandler* jgmh = new JoinGameMessageHandler(new DefaultHandler());
        Response *response = jgmh->handleMessage(&jgm, serverThread);

        JoinGameResponse* jgr = dynamic_cast<JoinGameResponse*>(response);
        REQUIRE(jgr != nullptr);

        REQUIRE(jgr->getJoined() == false);
        REQUIRE(jgr->getSuccess() == false);
        REQUIRE(jgr->getColor() == Color::Invalid);
        REQUIRE(jgr->getErrorMessage() == "Partija je puna!");

    }
}
