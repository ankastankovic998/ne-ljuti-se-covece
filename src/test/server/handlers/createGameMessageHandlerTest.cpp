#include "../../catch.hpp"
#include "../../../common/message.h"
#include "../../../server/participants.h"
#include "../../../server/handlers.h"
#include "../../../server/managers.h"


TEST_CASE("CreateGameHandlerTest", "[CreateGameMessageHandler]")
{

    SECTION("CreateGameHandlerTest: Uspesno kreiranje igre")
    {
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        CreateGameMessage cgm(4);

        CreateGameMessageHandler* cgmh = new CreateGameMessageHandler(new DefaultHandler());
        Response *response = cgmh->handleMessage(&cgm, serverThread);

        CreateGameResponse* cgr = dynamic_cast<CreateGameResponse*>(response);
        REQUIRE(cgr != nullptr);

        REQUIRE(cgr->getSuccess() == true);
        REQUIRE(cgr->getColor() == Color::Blue);
        REQUIRE(!cgr->getGameCode().isEmpty());
        REQUIRE(cgr->shouldBroadcast() == false);

        REQUIRE(serverThread->gameManager != nullptr);
    }

    SECTION("CreateGameHandlerTest: Partija vec napravljena")
    {
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        CreateGameMessage cgm(4);

        CreateGameMessageHandler* cgmh = new CreateGameMessageHandler(new DefaultHandler());
        Response *response = cgmh->handleMessage(&cgm, serverThread);

        CreateGameResponse* cgr = dynamic_cast<CreateGameResponse*>(response);
        REQUIRE(cgr != nullptr);

        REQUIRE(cgr->getSuccess() == true);
        REQUIRE(cgr->getColor() == Color::Blue);
        REQUIRE(!cgr->getGameCode().isEmpty());
        REQUIRE(cgr->shouldBroadcast() == false);

        REQUIRE(serverThread->gameManager != nullptr);


        CreateGameMessage cgm1(4);
        Response *response1 = cgmh->handleMessage(&cgm1, serverThread);

        CreateGameResponse* cgr1 = dynamic_cast<CreateGameResponse*>(response1);
        REQUIRE(cgr1 != nullptr);

        REQUIRE(cgr1->getSuccess() == false);
        REQUIRE(cgr1->shouldBroadcast() == false);
        REQUIRE(cgr1->getErrorMessage() == "Partija vec napravljena! Ne mozete ponovo kreirati partiju.");
    }

    SECTION("CreateGameHandlerTest: Partija sa dva ljudska igraca")
    {
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        CreateGameMessage cgm(2);

        CreateGameMessageHandler* cgmh = new CreateGameMessageHandler(new DefaultHandler());
        Response *response = cgmh->handleMessage(&cgm, serverThread);

        CreateGameResponse* cgr = dynamic_cast<CreateGameResponse*>(response);
        REQUIRE(cgr != nullptr);

        REQUIRE(cgr->getSuccess() == true);
        REQUIRE(cgr->getColor() == Color::Blue);
        REQUIRE(!cgr->getGameCode().isEmpty());
        REQUIRE(cgr->shouldBroadcast() == false);

        REQUIRE(serverThread->gameManager != nullptr);

        QList<BaseParticipant *> particepants = serverThread->gameManager->getParticepants();
        REQUIRE(particepants.count() == 3);

        ServerThreadParticipant* p1 = dynamic_cast<ServerThreadParticipant*>(particepants[0]);
        REQUIRE(p1 != nullptr);

        Bot* p2 = dynamic_cast<Bot*>(particepants[1]);
        REQUIRE(p2 != nullptr);
        REQUIRE(p2->getColor() == Color::Red);

        Bot* p3 = dynamic_cast<Bot*>(particepants[2]);
        REQUIRE(p3 != nullptr);
        REQUIRE(p3->getColor() == Color::Green);

    }

    SECTION("CreateGameHandlerTest: Partija sa jednim ljudskim igracem")
    {
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        CreateGameMessage cgm(1);

        CreateGameMessageHandler* cgmh = new CreateGameMessageHandler(new DefaultHandler());
        Response *response = cgmh->handleMessage(&cgm, serverThread);

        CreateGameResponse* cgr = dynamic_cast<CreateGameResponse*>(response);
        REQUIRE(cgr != nullptr);

        REQUIRE(cgr->getSuccess() == true);
        REQUIRE(cgr->getColor() == Color::Blue);
        REQUIRE(!cgr->getGameCode().isEmpty());
        REQUIRE(cgr->shouldBroadcast() == false);

        REQUIRE(serverThread->gameManager != nullptr);

        QList<BaseParticipant *> particepants = serverThread->gameManager->getParticepants();
        REQUIRE(particepants.count() == 4);

        ServerThreadParticipant* p1 = dynamic_cast<ServerThreadParticipant*>(particepants[0]);
        REQUIRE(p1 != nullptr);

        Bot* p2 = dynamic_cast<Bot*>(particepants[1]);
        REQUIRE(p2 != nullptr);
        REQUIRE(p2->getColor() == Color::Red);
        REQUIRE(serverThread->gameManager->playerReadyList[1] == true);

        Bot* p3 = dynamic_cast<Bot*>(particepants[2]);
        REQUIRE(p3 != nullptr);
        REQUIRE(p3->getColor() == Color::Green);
        REQUIRE(serverThread->gameManager->playerReadyList[2] == true);

        Bot* p4 = dynamic_cast<Bot*>(particepants[3]);
        REQUIRE(p4 != nullptr);
        REQUIRE(p4->getColor() == Color::Yellow);
        REQUIRE(serverThread->gameManager->playerReadyList[3] == true);
    }

    SECTION("CreateGameHandlerTest: Partija sa pet ljudska igraca")
    {
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        CreateGameMessage cgm(5);

        CreateGameMessageHandler* cgmh = new CreateGameMessageHandler(new DefaultHandler());
        Response *response = cgmh->handleMessage(&cgm, serverThread);

        CreateGameResponse* cgr = dynamic_cast<CreateGameResponse*>(response);
        REQUIRE(cgr != nullptr);

        REQUIRE(cgr->getSuccess() == false);
        REQUIRE(cgr->shouldBroadcast() == false);
        REQUIRE(cgr->getErrorMessage() == "Nevalidan broj igraca za partiju.");

        REQUIRE(serverThread->gameManager == nullptr);
    }

}
