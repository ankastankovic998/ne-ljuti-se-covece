#include "../../catch.hpp"
#include "../../../common/message.h"
#include "../../../server/participants.h"
#include "../../../server/server.h"
#include "../../../server/handlers.h"
#include "../../../server/managers.h"

TEST_CASE("MovePawnMessageHandlerTest", "[MovePawnMessageHandler]")
{

    SECTION("MovePawnMessageHandlerTest: Valid MovePawnMessage Handling")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = gameManager;
        serverThread->color = Color::Blue;

        gameManager->getTurnContext()->setCurrentPlayerColor(Color::Blue);
        gameManager->getTurnContext()->useOneRoll();
        gameManager->getTurnContext()->setLastRolledValue(6);

        MovePawnMessageHandler *handler = new MovePawnMessageHandler(new DefaultHandler());

        MovePawnMessage validMessage = MovePawnMessage("blue4", Color::Blue);
        Response* response = handler->handleMessage(&validMessage, serverThread);
        REQUIRE(response != nullptr);

        MovePawnResponse* moveResponse = dynamic_cast<MovePawnResponse*>(response);
        REQUIRE(moveResponse->getEatenColor() == Color::Invalid);
        REQUIRE(moveResponse->getErrorMessage() == "");
        delete handler;
        delete response;
    }

    SECTION("MovePawnMessageHandlerTest: Invalid MovePawnMessage Handling")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = gameManager;
        serverThread->color = Color::Blue;

        MovePawnMessageHandler *handler = new MovePawnMessageHandler(new DefaultHandler());

        MovePawnMessage invalidMessage("f22", Color::Red);
        Response* response = handler->handleMessage(&invalidMessage, serverThread);
        REQUIRE(response != nullptr);

        MovePawnResponse* moveResponse = dynamic_cast<MovePawnResponse*>(response);
        REQUIRE(moveResponse->getErrorMessage() == "Ne mozete pomeriti figuricu jer nije vasa!");

        delete handler;
        delete response;
    }

    SECTION("MovePawnMessageHandlerTest: MovePawnMessage Handling with Valid Roll and Turn Context")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        gameManager->board = new Board(4);
        gameManager->board->addPlayer(Color::Blue);
        QString field = "f10";
        int fieldId = field.mid(1).toInt() - 1;
        gameManager->board->table[fieldId]->updateSquare(Color::Blue, 1);
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = gameManager;
        serverThread->color = Color::Blue;
        serverThread->gameManager->getTurnContext()->setLastRolledValue(2);


        MovePawnMessageHandler *handler = new MovePawnMessageHandler(new DefaultHandler());

        MovePawnMessage validMessage = MovePawnMessage(field, Color::Blue);
        Response* response = handler->handleMessage(&validMessage, serverThread);
        REQUIRE(response != nullptr);

        MovePawnResponse* moveResponse = dynamic_cast<MovePawnResponse*>(response);
        REQUIRE(moveResponse->getErrorMessage() == "");
        REQUIRE(moveResponse->getNewField() == "f12");
        REQUIRE(moveResponse->getOldField() == "f10");
        REQUIRE(moveResponse->getSuccess() == true);
        REQUIRE(moveResponse->getColor() == Color::Blue);

        delete handler;
        delete response;
    }

    SECTION("MovePawnMessageHandlerTest: MovePawnMessage Handling with Invalid Roll and Turn Context")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = gameManager;
        serverThread->gameManager->getTurnContext()->setLastRolledValue(0);
        serverThread->gameManager->getTurnContext()->setRemainingNumberOfMoves(1);
        serverThread->color = Color::Green;

        MovePawnMessageHandler *handler = new MovePawnMessageHandler(new DefaultHandler());

        MovePawnMessage invalidMessage = MovePawnMessage("f10", Color::Green);
        Response* response = handler->handleMessage(&invalidMessage, serverThread);
        REQUIRE(response != nullptr);

        MovePawnResponse* moveResponse = dynamic_cast<MovePawnResponse*>(response);
        REQUIRE(moveResponse->getErrorMessage() == "Morate baciti kockicu pre pomeranja figure!");

        delete serverThread;
        delete response;
    }

    SECTION("MovePawnMessageHandlerTest: MovePawnMessage Handling with Invalid Numbers of Move")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = gameManager;
        serverThread->gameManager->getTurnContext()->setRemainingNumberOfMoves(0);
        serverThread->color = Color::Green;

        MovePawnMessageHandler *handler = new MovePawnMessageHandler(new DefaultHandler());

        MovePawnMessage invalidMessage = MovePawnMessage("f10", Color::Green);
        Response* response = handler->handleMessage(&invalidMessage, serverThread);
        REQUIRE(response != nullptr);

        MovePawnResponse* moveResponse = dynamic_cast<MovePawnResponse*>(response);
        REQUIRE(moveResponse->getErrorMessage() == "Vec ste pomerili figuricu u ovom krugu!");

        delete handler;
        delete response;
    }
}
