#include "../catch.hpp"
#include "../../server/handlers.h"
#include "../../server/managers.h"


TEST_CASE("MessageManagerTest", "[MessageManagerTest]")
{
    MessageManager* messageManager = new MessageManager(nullptr);

    SECTION("MessageManagerTest - provera dostupnosti DefaultHandlera")
    {
        ChainElement* head = messageManager->getChainElement();
        bool found = false;

        while(head->nextElement != nullptr)
        {
            DefaultHandler* current = dynamic_cast<DefaultHandler*>(head);

            if(current != nullptr)
            {
                found = true;
            }
            head = head->nextElement;
        }

        // DefaultHandler mora biti samo na poslednjem mestu u chain-u, i ne treba da postoji vise od jednom
        REQUIRE(found == false);

        DefaultHandler* last = dynamic_cast<DefaultHandler*>(head);

        if(last != nullptr)
        {
            found = true;
        }

        REQUIRE(found == true);
    }

    SECTION("MessageManagerTest - provera dostupnosti CreateGameMessageHandlera")
    {
        ChainElement* head = messageManager->getChainElement();
        bool found = false;

        while(head != nullptr)
        {
            CreateGameMessageHandler* current = dynamic_cast<CreateGameMessageHandler*>(head);

            if(current != nullptr)
            {
                found = true;
            }
            head = head->nextElement;
        }
        REQUIRE(found == true);
    }

    SECTION("MessageManagerTest - provera dostupnosti JoinGameMessageHandlera")
    {
        ChainElement* head = messageManager->getChainElement();
        bool found = false;

        while(head != nullptr)
        {
            JoinGameMessageHandler* current = dynamic_cast<JoinGameMessageHandler*>(head);

            if(current != nullptr)
            {
                found = true;
            }
            head = head->nextElement;
        }
        REQUIRE(found == true);
    }

    SECTION("MessageManagerTest - provera dostupnosti PlayerReadyMessageHandlera")
    {
        ChainElement* head = messageManager->getChainElement();
        bool found = false;

        while(head != nullptr)
        {
            PlayerReadyMessageHandler* current = dynamic_cast<PlayerReadyMessageHandler*>(head);

            if(current != nullptr)
            {
                found = true;
            }
            head = head->nextElement;
        }
        REQUIRE(found == true);
    }

    SECTION("MessageManagerTest - provera dostupnosti StartGameMessageHandlera")
    {
        ChainElement* head = messageManager->getChainElement();
        bool found = false;

        while(head != nullptr)
        {
            StartGameMessageHandler* current = dynamic_cast<StartGameMessageHandler*>(head);

            if(current != nullptr)
            {
                found = true;
            }
            head = head->nextElement;
        }
        REQUIRE(found == true);
    }

    SECTION("MessageManagerTest - provera dostupnosti RollDiceMessageHandlera")
    {
        ChainElement* head = messageManager->getChainElement();
        bool found = false;

        while(head != nullptr)
        {
            RollDiceMessageHandler* current = dynamic_cast<RollDiceMessageHandler*>(head);

            if(current != nullptr)
            {
                found = true;
            }
            head = head->nextElement;
        }
        REQUIRE(found == true);
    }

    SECTION("MessageManagerTest - provera dostupnosti MovePawnMessageHandlera")
    {
        ChainElement* head = messageManager->getChainElement();
        bool found = false;

        while(head != nullptr)
        {
            MovePawnMessageHandler* current = dynamic_cast<MovePawnMessageHandler*>(head);

            if(current != nullptr)
            {
                found = true;
            }
            head = head->nextElement;
        }
        REQUIRE(found == true);
    }

    SECTION("MessageManagerTest - provera dostupnosti EndTurnMessageHandlera")
    {
        ChainElement* head = messageManager->getChainElement();
        bool found = false;

        while(head != nullptr)
        {
            EndTurnMessageHandler* current = dynamic_cast<EndTurnMessageHandler*>(head);

            if(current != nullptr)
            {
                found = true;
            }
            head = head->nextElement;
        }
        REQUIRE(found == true);
    }

    SECTION("MessageManagerTest - provera dostupnosti ActivateMagicMessageHandlera")
    {
        ChainElement* head = messageManager->getChainElement();
        bool found = false;

        while(head != nullptr)
        {
            ActivateMagicMessageHandler* current = dynamic_cast<ActivateMagicMessageHandler*>(head);

            if(current != nullptr)
            {
                found = true;
            }
            head = head->nextElement;
        }
        REQUIRE(found == true);
    }
}
