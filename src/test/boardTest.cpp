#include "catch.hpp"
#include "../server/board.h"

#include <QPair>

TEST_CASE("BoardTest", "[BoardMessageHandler]")
{

    SECTION("BoardTest: Uspesno kreiranje table")
    {
        Board *board = new Board(4);

        REQUIRE(board != nullptr);

        Player * plavi = board->getPlayer(Color::Blue) ;
        REQUIRE( plavi != nullptr );
        REQUIRE( plavi->getColor() == Color::Blue);

        Player * crveni = board->getPlayer(Color::Red) ;
        REQUIRE( crveni != nullptr );
        REQUIRE( crveni->getColor() == Color::Red);

        Player * zeleni = board->getPlayer(Color::Green) ;
        REQUIRE( zeleni != nullptr );
        REQUIRE( zeleni->getColor() == Color::Green);

        Player * zuti = board->getPlayer(Color::Yellow) ;
        REQUIRE( zuti != nullptr );
        REQUIRE( zuti->getColor() == Color::Yellow);

    }

    SECTION("BoardTest: Validacija izlazenje  piuna iz kucice")
    {
        Board *board = new Board(4);

        REQUIRE ( board->isPawnMoveValid("red1", Color::Red, 1 ) == false);
        REQUIRE ( board->isPawnMoveValid("red1", Color::Red, 2 ) == false);

        REQUIRE ( board->isPawnMoveValid("red1", Color::Red, 6 ) == true);
        REQUIRE ( board->isPawnMoveValid("green1", Color::Green, 6 ) == true);
        REQUIRE ( board->isPawnMoveValid("yellow1", Color::Yellow, 6 ) == true);
        REQUIRE ( board->isPawnMoveValid("blue1", Color::Blue, 6 ) == true);
    }

    SECTION("BoardTest: Validacija kretanje piuna")
    {
        Board *board = new Board(4);

        REQUIRE ( board->isPawnMoveValid("red1", Color::Red, 6 ) == true);

        QPair<QString, QString> pair = board->makePawnMove("red1", Color::Red, 6 );

        REQUIRE(pair.first == "f1");
        REQUIRE(pair.second == "" );
        Pawn * p1 = board->getPlayer(Color::Red)->getPawnByIdx(0);
        REQUIRE(p1->getSquare()->getName() =="f1");
        Square * s1 = board->getPlayer(Color::Red)->getPawnByIdx(0)->getSquare();
        REQUIRE(s1->getColor() == Color::Red);
        REQUIRE(s1->getPawnIdx() == 0);
    }

    SECTION("BoardTest: Validacija kretanje piuna")
    {
        Board *board = new Board(4);

        REQUIRE ( board->isPawnMoveValid("red1", Color::Red, 6 ) == true);
        QPair<QString, QString> pair = board->makePawnMove("red1", Color::Red, 6 );
        REQUIRE ( board->isPawnMoveValid("f1", Color::Red, 5 ) == true);
        pair = board->makePawnMove("f1", Color::Red, 5 );

        Pawn * p1 = board->getPlayer(Color::Red)->getPawnByIdx(0);
        Square * s1 = board->getPlayer(Color::Red)->getPawnByIdx(0)->getSquare();
        REQUIRE(s1->getColor() == Color::Red);
        REQUIRE(s1->getPawnIdx() == 0);
    }
    SECTION("BoardTest: Da li postoji dozvoljen potez")
    {
        Board *board = new Board(4);


        REQUIRE(board->hasValidPawnMoves(Color::Red,  6) == true );
        REQUIRE(board->hasValidPawnMoves(Color::Red,  3) == false );
        REQUIRE(board->hasValidPawnMoves(Color::Red,  1) == false );

        REQUIRE ( board->isPawnMoveValid("red1", Color::Red, 6 ) == true);
        QPair<QString, QString> pair = board->makePawnMove("red1", Color::Red, 6 );


        REQUIRE(board->hasValidPawnMoves(Color::Red,  6) == true );
        REQUIRE(board->hasValidPawnMoves(Color::Red,  3) == true );
        REQUIRE(board->hasValidPawnMoves(Color::Red,  1) == true );
    }
}
