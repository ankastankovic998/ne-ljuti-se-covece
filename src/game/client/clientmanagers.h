#ifndef CLIENTMANAGERS_H
#define CLIENTMANAGERS_H

#include <QObject>
#include "handlers.h"

class ClientMessageManager : public QObject
{
    Q_OBJECT
public:
    explicit ClientMessageManager(QObject *parent = nullptr);

    void handleMessage(const QByteArray &rawMessage);

private:
    ClientChainElement* chainElement;

signals:

};

#endif // CLIENTMANAGERS_H
