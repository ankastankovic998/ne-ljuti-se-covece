#include "client.h"
#include "../../common/message.h"
#include "../../common/messagefactory.h"

#include <QJsonObject>
#include <QJsonDocument>
#include <thread>
#include <iostream>
#include <QDebug>

Client* Client::instance = nullptr;

Client::Client(QObject *parent) :
    QThread(parent)
{
    this->socket = new QTcpSocket();
    this->messageManager = new ClientMessageManager(this);
    this->backgroundThreadStarted = false;
}

bool Client::getBackgroundThreadStarted() const
{
    return backgroundThreadStarted;
}

void Client::connectToServer() const
{
    this->socket->connectToHost(QHostAddress::LocalHost, 12345);
    if(this->socket->waitForConnected(3000)) {
        qDebug() << "Connection to server suceeded!";
    } else {
        qDebug() << "Connection to server failed!";
    }
}

Client *Client::getInstance()
{
    if (instance == nullptr){
        instance = new Client();
        instance->connectToServer();
    }

    return instance;
}

void Client::run()
{
    this->backgroundThreadStarted = true;

    connect(socket, SIGNAL(readyRead()), this, SLOT(readResponse()), Qt::DirectConnection);
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()), Qt::DirectConnection);

    exec();
}

Response* Client::sendWithResponse(Message &message)
{
    if (this->backgroundThreadStarted)
    {
        throw std::runtime_error("Startovan je thread koji handluje svaki response, koristite metodu Client::send(Message &message) umesto Response* Client::sendWithResponse(Message &message)");
    }

    this->socket->write(message.prepareMessage());
    this->socket->waitForBytesWritten(1000);

    this->socket->waitForReadyRead(20000);
    return (Response*)MessageFactory::createMessage(socket->readAll());
}

void Client::send(Message &message)
{
    this->socket->write(message.prepareMessage());
    this->socket->waitForBytesWritten(1000);
}

void Client::readResponse()
{
    try {
        QByteArray rawServerResponse = socket->readAll();

        this->messageManager->handleMessage(rawServerResponse);

    } catch (...) {
        qDebug() << "Unknown exception";
    }
}

void Client::disconnected()
{
    socket->deleteLater();
    exit(0);
}
