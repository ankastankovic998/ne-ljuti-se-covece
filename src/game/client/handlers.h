#ifndef HANDLERS_H
#define HANDLERS_H

#include <QObject>
#include "../../common/message.h"


class ClientChainElement : public QObject
{
public:
    ClientChainElement* nextElement;

    explicit ClientChainElement(ClientChainElement* nextElement);

    virtual void handleResponse(Response* response) = 0;

    static QString enumValue(Color color);
};

class DefaultResponseHandler : public ClientChainElement
{
public:

    explicit DefaultResponseHandler(ClientChainElement* nextElement = nullptr)
        : ClientChainElement {nextElement} {};

    void handleResponse(Response* response)override;
};

class CreateGameResponseHandler : public ClientChainElement
{
public:

    explicit CreateGameResponseHandler(ClientChainElement* nextElement = nullptr)
        : ClientChainElement {nextElement} {};

    void handleResponse(Response* response) override;
};

class JoinGameResponseHandler : public ClientChainElement
{
public:

    explicit JoinGameResponseHandler(ClientChainElement* nextElement)
        : ClientChainElement {nextElement} {};

    void handleResponse(Response* response) override;
};

class PlayerReadyResponseHandler : public ClientChainElement
{
public:

    explicit PlayerReadyResponseHandler(ClientChainElement* nextElement)
        : ClientChainElement {nextElement} {};

    void handleResponse(Response* response) override;
};

class StartGameResponseHandler : public ClientChainElement
{
public:

    explicit StartGameResponseHandler(ClientChainElement* nextElement)
        : ClientChainElement {nextElement} {};

    void handleResponse(Response* response) override;
};


class EndTurnResponseHandler : public ClientChainElement
{
public:

    explicit EndTurnResponseHandler(ClientChainElement* nextElement)
        : ClientChainElement {nextElement} {};

    void handleResponse(Response* response) override;
};

class RollDiceResponseHandler : public ClientChainElement
{
public:

    explicit RollDiceResponseHandler(ClientChainElement* nextElement)
        : ClientChainElement {nextElement} {};

    void handleResponse(Response* response) override;
};

class EndGameResponseHandler : public ClientChainElement
{
public:

    explicit EndGameResponseHandler(ClientChainElement* nextElement)
        : ClientChainElement {nextElement} {};

    void handleResponse(Response* response) override;

};

class MovePawnResponseHandler : public ClientChainElement
{
public:

    explicit MovePawnResponseHandler(ClientChainElement* nextElement)
        : ClientChainElement {nextElement} {};

    void handleResponse(Response* response) override;
};


class ActivateMagicResponseHandler : public ClientChainElement
{
public:

    explicit ActivateMagicResponseHandler(ClientChainElement* nextElement)
        : ClientChainElement {nextElement} {};

    void handleResponse(Response* response) override;
};



#endif // HANDLERS_H
