#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../client/client.h"
#include "maingame.h"


#include <QWidget>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum MWPageIndex{
    FirstPage,
    CreatePage,
    JoinPage,
    RulePage,
    WaitingPage,
    CodePage
};

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    ~MainWindow();
    static MainWindow* getInstance();

    void transitionToCodePage(QString gameCode);
    void transitionToLoadingPage();

private slots:
    void on_createGameButton_clicked();
    void on_joinGameButton_clicked();
    void on_joinpage_returnToFirstPage_clicked();
    void on_createpage_returnToFirstPage_clicked();
    void on_rulepage_returnToFirstPage_clicked();
    void on_gameRulesButton_clicked();
    void on_readyButton_clicked();
    void on_startGameButton_clicked();
    void on_joinButton_clicked();
    void on_generateCodeButton_clicked();

private:
    Ui::MainWindow *ui;

    MainWindow(QWidget *parent = nullptr);

    static MainWindow *instance;
    int getNumberOfPlayers() const;
};
#endif // MAINWINDOW_H
