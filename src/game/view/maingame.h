#ifndef MAINGAME_H
#define MAINGAME_H

#include <QWidget>
#include <QGraphicsScene>
#include "../client/client.h"

namespace Ui {
class MainGame;
}

class MainGame : public QWidget
{
    Q_OBJECT

public:
    ~MainGame();
    static MainGame* getInstance();
    void setPlayerColor(Color color);
    void changeDiceValue(int br);
    void movePawn(Color color, QString newField);
    void changeToTransparent(QString field);
    void updateMagicNumber(QString field, int num);
    void updateInfoLabel(QString info);
    void placeShieldMagic(Color color);
    void removeShieldMagic(Color color);

private slots:
    void on_rollDiceBtn_clicked();
    void on_endTurnbtn_clicked();


private:
    static MainGame* instance;
    Ui::MainGame *ui;
    Color color;
    explicit MainGame(QWidget *parent = nullptr);

    void playerColor(Color color);
    void remoteDiceMove(QString fieldName);
    QList<QString> generateHouseButtons(Color color);
    void on_magicbtn_clicked();
    void on_fieldbtn_clicked();
};

#endif // MAINGAME_H
