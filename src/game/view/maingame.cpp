#include "../view/maingame.h"
#include "./ui_maingame.h"
#include <qstring.h>
#include <QRegularExpression>
#include <QDialog>
#include <QRadioButton>
#include <QVBoxLayout>


MainGame* MainGame::instance = nullptr;

MainGame* MainGame::getInstance()
{
    if(instance == nullptr){
        instance = new MainGame();
    }
    return instance;
}


void MainGame::setPlayerColor(Color color)
{
    this->setWindowTitle(ClientChainElement::enumValue(color));
    playerColor(color);
}

MainGame::MainGame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainGame)
{
    ui->setupUi(this);

    //izlazenje sa starta:
    connect(ui->green1,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->green2,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->green4,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->green3,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);

    connect(ui->yellow1,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->yellow2,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->yellow3,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->yellow4,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);

    connect(ui->red1,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->red2,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->red3,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->red4,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);

    connect(ui->blue1,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->blue2,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->blue3,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->blue4,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);

    //biranje magije:
    connect(ui->shield,&QPushButton::clicked,this,&MainGame::on_magicbtn_clicked,Qt::QueuedConnection);
    connect(ui->double_dice,&QPushButton::clicked,this,&MainGame::on_magicbtn_clicked,Qt::QueuedConnection);
    connect(ui->plus_1,&QPushButton::clicked,this,&MainGame::on_magicbtn_clicked,Qt::QueuedConnection);
    connect(ui->minus_1,&QPushButton::clicked,this,&MainGame::on_magicbtn_clicked,Qt::QueuedConnection);
    connect(ui->remote_dice,&QPushButton::clicked,this,&MainGame::on_magicbtn_clicked,Qt::QueuedConnection);

    //menjanje pozicije:
    connect(ui->f1,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f2,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f3,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f4,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f5,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f6,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f7,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f8,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f9,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f10,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f11,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f12,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f13,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f14,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f15,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f16,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f17,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f18,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f19,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f20,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f21,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f22,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f23,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f24,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f25,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f26,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f27,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f28,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f29,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f30,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f31,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f32,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f33,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f34,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f35,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f36,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f37,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f38,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f39,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f40,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f41,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f42,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f43,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f44,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f45,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f46,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f47,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f48,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f49,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f50,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f51,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->f52,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);

    connect(ui->kr1,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->kr2,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->kr4,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->kr3,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);

    connect(ui->kb1,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->kb2,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->kb3,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->kb4,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);

    connect(ui->ky1,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->ky2,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->ky3,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->ky4,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);

    connect(ui->kg1,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->kg2,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->kg3,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);
    connect(ui->kg4,&QPushButton::clicked,this,&MainGame::on_fieldbtn_clicked,Qt::QueuedConnection);

}

MainGame::~MainGame()
{
    delete ui;
}


void MainGame::on_rollDiceBtn_clicked()
{
    RollDiceMessage msg;
    Client::getInstance()->send(msg);
}

void MainGame::changeDiceValue(int br)
{
    if(br==1){
        ui->kockica->setStyleSheet("border-image:url(:/media/dice_1.png)");
    }
    else if(br==2){
        ui->kockica->setStyleSheet("border-image:url(:/media/dice_2.png)");
    }
    else if(br==3){
        ui->kockica->setStyleSheet("border-image:url(:/media/dice_3.png)");
    }
    else if(br==4){
        ui->kockica->setStyleSheet("border-image:url(:/media/dice_4.png)");
    }
    else if(br==5){
        ui->kockica->setStyleSheet("border-image:url(:/media/dice_5.png)");
    }
    else if(br==6){
        ui->kockica->setStyleSheet("border-image:url(:/media/dice_6.png)");
    }
}

void MainGame::playerColor(Color color)
{
    if (color == Red){
        ui->currentColor->setStyleSheet("border-image:url(:/media/red1.png)");
    }
    else if(color == Green){
        ui->currentColor->setStyleSheet("border-image:url(:/media/green.png)");
    }
    else if(color == Yellow){
        ui->currentColor->setStyleSheet("border-image:url(:/media/yellow.png)");
    }
    else if(color == Blue){
        ui->currentColor->setStyleSheet("border-image:url(:/media/blue.png)");
    }
}

void MainGame::movePawn(Color color, QString newField)
{
    QPushButton* foundButton = MainGame::getInstance()->findChild<QPushButton*>(newField);

    if (color == Red){
        foundButton->setStyleSheet("border-image: url(:/media/red1.png);");
    }
    else if(color == Green){
        foundButton->setStyleSheet("border-image: url(:/media/green.png);");
    }
    else if(color == Yellow){
        foundButton->setStyleSheet("border-image: url(:/media/yellow.png);");
    }
    else if(color == Blue){
        foundButton->setStyleSheet("border-image: url(:/media/blue.png);");
    }
}

void MainGame::changeToTransparent(QString field)
{
    QPushButton* foundButton = MainGame::getInstance()->findChild<QPushButton*>(field);
    foundButton->setStyleSheet("border-image: url(:/media/transparent.png);");
}

void MainGame::updateMagicNumber(QString field, int num)
{
    field.append("_num");
    QLabel* magicLabel = MainGame::getInstance()->findChild<QLabel*>(field);
    magicLabel->setText(QString::number(num));
}

void MainGame::updateInfoLabel(QString info)
{
    QLabel* infoLabel = MainGame::getInstance()->findChild<QLabel*>("infolabel");
    infoLabel->setText(info);
}

//end turn
void MainGame::on_endTurnbtn_clicked()
{
    EndTurnMessage etm;
    Client::getInstance()->send(etm);
}

void MainGame::on_magicbtn_clicked()
{
    QPushButton *field = qobject_cast<QPushButton*>(sender());
    QString fieldName = field->objectName();
    if (fieldName == "remote_dice") {
        remoteDiceMove(fieldName);
    }
    else {
        ActivateMagicMessage msg(fieldName);
        Client::getInstance()->send(msg);
    }
}

void MainGame::on_fieldbtn_clicked()
{
    QPushButton *field = qobject_cast<QPushButton*>(sender());
    QString fieldName = field->objectName();
    QString buttonStyleSheet = field->styleSheet();
    static QRegularExpression regex("url\\(:/media/(.*?)\\.png\\);");
    QRegularExpressionMatch match = regex.match(buttonStyleSheet);
    if (match.hasMatch())
    {
        // Dobijanje podudarnosti (vrednosti između 'url(:/media/' i '.png);')
        QString value = match.captured(1);
        if(value[0] != 't') {
            Color color = Invalid;
            if(value[0] == 'b')
            {
                color = Blue;
            }
            else if(value[0] == 'r')
            {
                color = Red;
            }
            else if(value[0] == 'g')
            {
                color = Green;
            }
            else if(value[0] == 'y')
            {
                color = Yellow;
            }

            MovePawnMessage msg(fieldName, color);
            Client::getInstance()->send(msg);
        }

    }
    else
    {
        qDebug() << "Podudarnost nije pronađena.";
    }
}

void MainGame::remoteDiceMove(QString fieldName)
{
    QDialog *widget = new QDialog();
    widget->setWindowTitle("Odaberi broj na kockici");
    widget->setFixedSize(300, 200);

    // Kreiranje radio dugmica
    QRadioButton *radioBtn1 = new QRadioButton("1", widget);
    QRadioButton *radioBtn2 = new QRadioButton("2", widget);
    QRadioButton *radioBtn3 = new QRadioButton("3", widget);
    QRadioButton *radioBtn4 = new QRadioButton("4", widget);
    QRadioButton *radioBtn5 = new QRadioButton("5", widget);
    QRadioButton *radioBtn6 = new QRadioButton("6", widget);

    // Postavljanje rasporeda
    QVBoxLayout *layout = new QVBoxLayout(widget);
    layout->addWidget(radioBtn1);
    layout->addWidget(radioBtn2);
    layout->addWidget(radioBtn3);
    layout->addWidget(radioBtn4);
    layout->addWidget(radioBtn5);
    layout->addWidget(radioBtn6);

    widget->setLayout(layout);

    connect(radioBtn1, &QRadioButton::clicked, this, [=]() {
        ActivateMagicMessage msg(fieldName, qint32(1));
        Client::getInstance()->send(msg);
        widget->close();
    });

    connect(radioBtn2, &QRadioButton::clicked, this, [=]() {
        ActivateMagicMessage msg(fieldName, qint32(2));
        Client::getInstance()->send(msg);
        widget->close();
    });

    connect(radioBtn3, &QRadioButton::clicked, this, [=]() {
        ActivateMagicMessage msg(fieldName, qint32(3));
        Client::getInstance()->send(msg);
        widget->close();
    });

    connect(radioBtn4, &QRadioButton::clicked, this, [=]() {
        ActivateMagicMessage msg(fieldName, qint32(4));
        Client::getInstance()->send(msg);
        widget->close();
    });

    connect(radioBtn5, &QRadioButton::clicked, this, [=]() {
        ActivateMagicMessage msg(fieldName, qint32(5));
        Client::getInstance()->send(msg);
        widget->close();
    });

    connect(radioBtn6, &QRadioButton::clicked, this, [=]() {
        ActivateMagicMessage msg(fieldName, qint32(6));
        Client::getInstance()->send(msg);
        widget->close();
    });

    // Prikazivanje prozora
    widget->exec();
}

void MainGame::placeShieldMagic(Color color)
{
    QList<QString> houseButtons = generateHouseButtons(color);

    for (const QString &houseName : houseButtons) {
        QPushButton* foundButton = MainGame::getInstance()->findChild<QPushButton*>(houseName);
        QString styleSheet = foundButton->styleSheet();
        // Definiše regularni izraz koji traži željeni deo style-sheeta
        static QRegularExpression regex("border-image: url\\(:/media/transparent.png\\);");
        QRegularExpressionMatch match = regex.match(styleSheet);
        // Pronalazi prvu pojavu regularnog izraza u stilu
        if (match.hasMatch())
        {
            foundButton->setStyleSheet("border-image: url(:/media/magic1Shield.png);");
            break;
        }
        else
        {
            qDebug() << "Podudarnost nije pronađena.";
        }
    }
}

void MainGame::removeShieldMagic(Color color)
{
    QList<QString> houseButtons = generateHouseButtons(color);

    for (const QString &houseName : houseButtons) {
        QPushButton* foundButton = MainGame::getInstance()->findChild<QPushButton*>(houseName);
        QString styleSheet = foundButton->styleSheet();
        // Definiše regularni izraz koji traži željeni deo style-sheeta
        static QRegularExpression regex("border-image: url\\(:/media/magic1Shield.png\\);");
        QRegularExpressionMatch match = regex.match(styleSheet);
        // Pronalazi prvu pojavu regularnog izraza u stilu
        if (match.hasMatch())
        {
            foundButton->setStyleSheet("border-image: url(:/media/transparent.png);");
            break;
        }
    }
}

QList<QString> MainGame::generateHouseButtons(Color color)
{
    QList<QString> houseButtons;
    if(color == Color::Blue) {
        houseButtons = {"blue1", "blue2", "blue3", "blue4"};
    }
    else if(color == Color::Red) {
        houseButtons = {"red1", "red2", "red3", "red4"};
    }
    else if(color == Color::Green) {
        houseButtons = {"green1", "green2", "green3", "green4"};
    }
    else if(color == Color::Yellow) {
        houseButtons = {"yellow1", "yellow2", "yellow3", "yellow4"};
    }

    return houseButtons;
}
