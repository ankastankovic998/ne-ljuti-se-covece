#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow* MainWindow::instance = nullptr;

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(MWPageIndex::FirstPage);
}

MainWindow::~MainWindow()
{
    delete ui;
}

MainWindow *MainWindow::getInstance()
{
    if (instance == nullptr){
        instance = new MainWindow();
    }

    return instance;
}

void MainWindow::transitionToCodePage(QString gameCode)
{
    QLineEdit* generatedCode = this->ui->codepage->findChild<QLineEdit* >("generatedCode");
    generatedCode->setText(gameCode);
    generatedCode->setStyleSheet("color: rgb(0, 0, 127); font: 600 30pt 'Sitka Small Semibold'; border-image: url(:/media/bela.jpg)");

    ui->stackedWidget->setCurrentIndex(MWPageIndex::CodePage);
}

void MainWindow::transitionToLoadingPage()
{
    ui->stackedWidget->setCurrentIndex(MWPageIndex::WaitingPage);
}

void MainWindow::on_createGameButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(MWPageIndex::CreatePage);
}

void MainWindow::on_joinGameButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(MWPageIndex::JoinPage);
}

void MainWindow::on_gameRulesButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(MWPageIndex::RulePage);
}

void MainWindow::on_joinpage_returnToFirstPage_clicked()
{
    ui->stackedWidget->setCurrentIndex(MWPageIndex::FirstPage);
}

void MainWindow::on_createpage_returnToFirstPage_clicked()
{
    ui->stackedWidget->setCurrentIndex(MWPageIndex::FirstPage);
}

void MainWindow::on_rulepage_returnToFirstPage_clicked()
{
    ui->stackedWidget->setCurrentIndex(MWPageIndex::FirstPage);
}

void MainWindow::on_generateCodeButton_clicked()
{
    CreateGameMessage msg(this->getNumberOfPlayers());
    Response* response = Client::getInstance()->sendWithResponse(msg);

    CreateGameResponseHandler responseHandler;
    responseHandler.handleResponse(response);
    delete response;
}

int MainWindow::getNumberOfPlayers() const
{
    return this->ui->bgSelectPlayerNumber->checkedButton()->text().toInt();
}

void MainWindow::on_joinButton_clicked()
{
    QLineEdit* enterCode = this->ui->joinpage->findChild<QLineEdit* >("lineJoinCode");

    JoinGameMessage jgm(enterCode->text());
    Response* response = Client::getInstance()->sendWithResponse(jgm);

    JoinGameResponseHandler responseHandler(new DefaultResponseHandler);
    responseHandler.handleResponse(response);
    delete response;
}

void MainWindow::on_readyButton_clicked()
{
    PlayerReadyMessage msg;
    if(Client::getInstance()->getBackgroundThreadStarted())
    {
        Client::getInstance()->send(msg);
    }
    else
    {
        Response* response = Client::getInstance()->sendWithResponse(msg);
        PlayerReadyResponseHandler responseHandler(new DefaultResponseHandler);
        responseHandler.handleResponse(response);
        delete response;
    }
}

void MainWindow::on_startGameButton_clicked()
{
    StartGameMessage msg;
    Client::getInstance()->send(msg);
}
