#include "endgame.h"
#include "ui_endgame.h"

endGame* endGame::instance = nullptr;

endGame* endGame::getInstance()
{
    if(instance == nullptr){
        instance = new endGame();
    }
    return instance;
}

endGame::endGame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::endGame)
{
    ui->setupUi(this);
    this->setWindowTitle("End game");
}

endGame::~endGame()
{
    delete ui;
}

void endGame::showWinner(Color color)
{
    QLabel* winner = endGame::getInstance()->findChild<QLabel*>("winner");
    if (color == Red){
        winner->setStyleSheet("border-image: url(:/media/red1.png);");
    }
    else if(color == Green){
        winner->setStyleSheet("border-image: url(:/media/green.png);");
    }
    else if(color == Yellow){
        winner->setStyleSheet("border-image: url(:/media/yellow.png);");
    }
    else if(color == Blue){
        winner->setStyleSheet("border-image: url(:/media/blue.png);");
    }
}

