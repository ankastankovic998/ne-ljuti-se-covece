#include "client/client.h"
#include "view/mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Kreira instancu client i konektuje se na server
    Client::getInstance();

    // Prikazuje glavni prozor
    MainWindow::getInstance()->show();

    return a.exec();
}
