cmake_minimum_required(VERSION 3.5)

project(ne-ljuti-se-covece VERSION 0.1 LANGUAGES CXX)

add_subdirectory(src/common)
add_subdirectory(src/server)
add_subdirectory(src/game)
add_subdirectory(src/test)
